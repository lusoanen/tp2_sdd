#ifndef _LISTE_H_
#define _LISTE_H_

#include <stdlib.h>
#include <stdio.h>
#include "type.h"

typedef struct maillon
{
    elem_t elem;          //element de la cellule
    struct maillon *suiv; //poiteru sur la cellule suivante
} maillon_t;

typedef struct liste
{
    maillon_t *tete; //poiteru sur la cellule de tête de liste
} liste_t;

liste_t *creerListe();
void viderListe(liste_t *);
void supprimerElement(maillon_t **);
void insererListe(maillon_t **, elem_t);
void afficherListeInt(maillon_t *);

#endif