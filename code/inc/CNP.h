#ifndef _CNP_H_
#define _CNP_H_

#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

#define MIN_FCT(a, b) (((a) < (b)) ? (a) : (b))

int CNP_recur(int n, int p);
int CNP_iter(int n, int p);
int CNP_recur2(long int n, long int p);
int CNPPile(int n, int p);
int CNPDynamique(int n, int p);
void freeTab(int **tab, int ligne);


#endif