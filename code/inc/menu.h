#ifndef _MENU_H_
#define _MENU_H_

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <time.h>

#include "file.h"
#include "pile.h"
#include "CNP.h"

void afficherMenu();
void gestionPile();
void gestionFile();
void gestionRecur();

#endif