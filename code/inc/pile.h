#ifndef _PILE_H_
#define _PILE_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "type.h"
#include "liste.h"
#include "bool.h"

typedef liste_t pile_t;

pile_t *creerPile();
void libererPile(pile_t *);
void empiler(pile_t *, elem_t);
void depiler(pile_t *);
elem_t sommetPile(pile_t *);
bool_t estVide(pile_t *);
void afficherPile(pile_t *);

#endif