#ifndef _FILE_H_
#define _FILE_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "type.h"
#include "bool.h"

typedef struct file
{
    elem_t *tab;   //tableau contenant les élément de la file
    int deb;       //indice du tableau représentant le début de la file
    int fin;       //indice du tableau représentant la fin de la file
    int tailleTab; //taille du tableau
} file_t;

file_t *creerFile();
void enfiler(file_t *, elem_t);
void afficherFile(file_t);
bool_t estVideFile(file_t);
void defiler(file_t *);
elem_t queueFile(file_t);
void libererFile(file_t *);

#endif