#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/menu.h"

int main()
{

    //system("clear");

    int choix = 1;

    while (choix != 0)
    {

        afficherMenu();
        scanf("%d%*c", &choix);

        switch (choix)
        {
        case 1:
            gestionPile();
            break;
        case 2:
            gestionFile();
            break;
        case 3:
            gestionRecur();
            break;
        default:
            break;
        }
    }
}