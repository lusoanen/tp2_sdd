#include "../inc/CNP.h"

/****************************************************
* 
* CNP_recur(int n, int p)                          
*                                                   
* Implémentation récursive de la fonction CNP de 
* l'énoncé
*                                                   
* Input : deux entiers n et p                                          
* Output : le résultat de la fonction    
****************************************************/
int CNP_recur(int n, int p)
{

    if (n == p || p == 0)
    {
        return 1;
    }
    else
    {
        return CNP_recur(n - 1, p) + CNP_recur(n - 1, p - 1);
    }
}

/****************************************************
* CNP_iter(int n, int p)                        
*                                                   
* Implémentation itérative grâce aux propriétés
* mathématiques du coefficient binomial
*                                                   
* Input : deux entiers n et p                                          
* Output : le résultat de la fonction    
*****************************************************/
int CNP_iter(int n, int p)
{

    int x = 1;
    int y = 1;
    int i = n - p + 1;

    if (p > n / 2)
    {
        p = n - p;
    }

    while (i <= n)
    {
        x = (x * i) / y;
        y++;
        i++;
    }

    return x;
}

/****************************************************
* CNP_recur2(int n, int p)                        
*                                                   
* Implémentation récursive avec récursivité terminale
*                                                   
* Input : deux entiers n et p                                          
* Output : le résultat de la fonction    
*****************************************************/
int CNP_recur2(long int n, long int p)
{
    if (p == 0)
    {
        return 1;
    }
    else
    {
        return (CNP_recur2(n - 1, p - 1) * n) / p;
    }
}

/****************************************************
* CNPPile(int n, int p)                      
*                                                   
* Implémentation itérative en imitant la méthode des
* piles du système.
*                                                   
* Input : deux entiers n et p                                          
* Output : le résultat de la fonction    
*****************************************************/
int CNPPile(int n, int p)
{
    pile_t *pile_n = creerPile();
    pile_t *pile_p = creerPile();
    int resultat = 0;
    int i, j;

    empiler(pile_n, n);
    empiler(pile_p, p);

    while (estVide(pile_p) == 0)
    {

        i = sommetPile(pile_n);
        j = sommetPile(pile_p);
        depiler(pile_n);
        depiler(pile_p);

        if (i == j || j == 0)
        {
            resultat++;
        }
        else
        {
            empiler(pile_n, i - 1);
            empiler(pile_n, i - 1);
            empiler(pile_p, j - 1);
            empiler(pile_p, j);
        }
    }

    libererPile(pile_n);
    libererPile(pile_p);

    return resultat;
}

/****************************************************
* int CNPDynamique(int n, int p)                     
*                                                   
* Implémentation itérative en se basant sur la
* programation dynamique. On ne calcul qu'une seule
* fois un élément
*                                                   
* Input : deux entiers n et p                                          
* Output : le résultat de la fonction    
*****************************************************/
int CNPDynamique(int n, int p)
{
    int i, j;
    int result;
    int **tab = NULL;
    tab = (int **)calloc((n + 1), sizeof(int *));
    if (tab == NULL)
    {
        fprintf(stderr, "Allocation mémoire impossible du tableau.\n");
        exit(1);
    }

    for (i = 0; i < n + 1; i++)
    {
        tab[i] = (int *)calloc((p + 1), sizeof(int));
        if (tab[i] == NULL)
        {
            freeTab(tab, i);
            fprintf(stderr, "Allocation mémoire impossible du tableau.\n");
            exit(1);
        }
    }

    for (i = 0; i <= n; i++)
    {
        for (j = 0; j <= MIN_FCT(i, p); j++)
        {
            if (i == 0 || j == 0)
            {
                tab[i][j] = 1;
            }
            else
            {
                tab[i][j] = tab[i - 1][j - 1] + tab[i - 1][j];
            }
        }
    }

    result = tab[n][p];
    freeTab(tab, n + 1);
    return result;
}

/****************************************************
* void freeTab(int **tab, int ligne)                    
*                                                   
* Permet la libération mémoire d'un tableau en deux
* dimension
*                                                   
* Input : un tableau et le nombre de lignes du tableau                                       
* Output : rien    
*****************************************************/
void freeTab(int **tab, int ligne)
{
    int i = 0;
    for (i = 0; i < ligne; i++)
    {
        free(tab[i]);
    }
    free(tab);
}