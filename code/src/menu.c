#include "../inc/menu.h"

/*****************************************************/
/* afficherMenu()                                    */
/*                                                   */
/* Affiche le menu pricipale                         */
/*                                                   */
/* Input :                                           */
/* Output :                                          */
/*****************************************************/

void afficherMenu()
{
	system("clear");
	printf("Bienvenue dans le menu principal. Veuillez selectionner une option :\n");
	printf("\t-------------------------------------------------\n");
	printf("\t| 1 - Menu de gestion de pile                   |\n");
	printf("\t| 2 - Menu de gestion de file                   |\n");
	printf("\t| 3 - Menu de gestion de dérécursification      |\n");
	printf("\t| 0 - Quitter le programme                      |\n");
	printf("\t-------------------------------------------------\n");
}

/*****************************************************/
/* afficherMenuPile()                                */
/*                                                   */
/* Affiche le sous menu d'action sur les piles.      */
/*                                                   */
/* Input :                                           */
/* Output :                                          */
/*****************************************************/

void afficherMenuPile()
{
	system("clear");
	printf("Bienvenue dans le menu de gestion de pile. Veuillez selectionner une option :\n");
	printf("\t-------------------------------------------------\n");
	printf("\t| 1 - Empiler une valeur                        |\n");
	printf("\t| 2 - Depiler                                   |\n");
	printf("\t| 3 - Montrer le sommet de la pile              |\n");
	printf("\t| 4 - Savoir si la pile est vide                |\n");
	printf("\t| 5 - Afficher la pile                          |\n");
	printf("\t| 0 - Revenir au menu principal                 |\n");
	printf("\t-------------------------------------------------\n");
}

/*****************************************************/
/* afficherMenuFile()                                */
/*                                                   */
/* Affiche le sous menu d'action sur les files.      */
/*                                                   */
/* Input :                                           */
/* Output :                                          */
/*****************************************************/

void afficherMenuFile()
{
	system("clear");
	printf("Bienvenue dans le menu de gestion de file. Veuillez selectionner une option :\n");
	printf("\t-------------------------------------------------\n");
	printf("\t| 1 - Enfiler une valeur                        |\n");
	printf("\t| 2 - Defiler                                   |\n");
	printf("\t| 3 - Montrer la queue de la file               |\n");
	printf("\t| 4 - Savoir si la file est vide                |\n");
	printf("\t| 5 - Afficher la file                          |\n");
	printf("\t| 0 - Revenir au menu principal                 |\n");
	printf("\t-------------------------------------------------\n");
}

/*****************************************************/
/* gestionPile()                                     */
/*                                                   */
/* Lance l'action sur la pile choisie par            */
/* l'utilisateur, par l'appel de la fonction associée*/
/*                                                   */
/* Input :                                           */
/* Output :                                          */
/*****************************************************/

void gestionPile()
{
	int choix = 1, val;
	pile_t *pile = creerPile();
	if (pile)
	{
		while (choix != 0)
		{
			afficherMenuPile();
			scanf("%d", &choix);

			switch (choix)
			{
			case 1:
				printf("\nEntrer la valeur a empiler (entier/int) :\n");
				scanf("%d", &val);
				empiler(pile, val);
				break;
			case 2:
				depiler(pile);
				break;
			case 3:
				printf("Tapez entrer une fois la lecture finit\n");
				if (estVide(pile) == 0)
					printf("Sommet : %d\n", sommetPile(pile));
				else
					printf("Pile vide\n");
				getchar();
				break;
			case 4:
				printf("Vide si 1, non vide si 0 : %d\n", estVide(pile));
				printf("Tapez entrer une fois la lecture finit\n");
				getchar();
				break;
			case 5:
				printf("Tapez entrer une fois la lecture finit\n");
				afficherPile(pile);
				getchar();
				break;
			default:
				break;
			}
			getchar();
		}
		libererPile(pile);
	}
}

/*****************************************************/
/* gestionFile()                                     */
/*                                                   */
/* Lance l'action sur la file choisie par            */
/* l'utilisateur, par l'appel de la fonction associée*/
/*                                                   */
/* Input :                                           */
/* Output :                                          */
/*****************************************************/

void gestionFile()
{
	int choix = 1, val;
	file_t *file = creerFile();
	if (file)
	{
		while (choix != 0)
		{
			afficherMenuFile();
			scanf("%d", &choix);

			switch (choix)
			{
			case 1:
				printf("\nEntrer la valeur a enfiler (entier/int) :\n");
				scanf("%d", &val);
				enfiler(file, val);
				break;
			case 2:
				defiler(file);
				break;
			case 3:
				printf("Tapez entrer une fois la lecture finit\n");
				if (estVideFile(*file) == 0)
					printf("Queue : %d\n", queueFile(*file));
				else
					printf("File vide\n");
				getchar();
				break;
			case 4:
				printf("Tapez entrer une fois la lecture finit\n");
				printf("Vide si 1, non vide si 0 : %d\n", estVideFile(*file));
				getchar();
				break;
			case 5:
				printf("Tapez entrer une fois la lecture finit\n");
				if (estVideFile(*file) == 0)
					afficherFile(*file);
				else
					printf("File vide\n");
				getchar();
				break;
			default:
				break;
			}
			getchar();
		}
		libererFile(file);
	}
}

/*****************************************************
* gestionRecur()                                    
* 
* Lance les différentes fonctions de test sur les 
* différentes implémentations de la fonction CNP
* 
* Input :                                           
* Output :                                          
*****************************************************/

void gestionRecur()
{
	clock_t tps_init, tps_fin;
	unsigned long tps_CPU;

	printf("Gestion de la dérécursifiction (Attention le temps de calcul et donc d'affichage peut être long, Veuillez patienter jusqu'à la fin) :\n");

	printf("\nAvec la fonction CNP de manière récursive : \n");
	tps_init = clock();
	printf("CNP_recur(5, 3) = %d\t\t", CNP_recur(5, 3));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur(12, 9) = %d\t\t", CNP_recur(12, 9));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur(15, 12) = %d\t\t", CNP_recur(15, 12));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur(20, 13) = %d\t", CNP_recur(20, 13));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur(27, 18) = %d\t", CNP_recur(27, 18));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur(35, 25) = %d\t", CNP_recur(35, 25));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	printf("\nAvec la fonction CNP de manière récursive mais sans récursivité terminale : \n");
	tps_init = clock();
	printf("CNP_recur2(5, 3) = %d\t\t", CNP_recur2(5, 3));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur2(12, 9) = %d\t\t", CNP_recur2(12, 9));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur2(15, 12) = %d\t", CNP_recur2(15, 12));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur2(20, 13) = %d\t", CNP_recur2(20, 13));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur2(27, 18) = %d\t", CNP_recur2(27, 18));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_recur2(35, 25) = %d\t", CNP_recur2(35, 25));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	printf("\nAvec la fonction CNP géré par la pile : \n");
	tps_init = clock();
	printf("CNPPile(5, 3) = %d\t\t", CNPPile(5, 3));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPPile(12, 9) = %d\t\t", CNPPile(12, 9));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPPile(15, 12) = %d\t\t", CNPPile(15, 12));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPPile(20, 13) = %d\t\t", CNPPile(20, 13));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPPile(27, 18) = %d\t", CNPPile(27, 18));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPPile(35, 25) = %d\t", CNPPile(35, 25));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	printf("\nAvec la fonction CNP de manière itérative grâce aux propriétées mathématiques : \n");
	tps_init = clock();
	printf("CNP_iter(5, 3) = %d\t\t", CNP_iter(5, 3));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_iter(12, 9) = %d\t\t", CNP_iter(12, 9));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_iter(15, 12) = %d\t\t", CNP_iter(15, 12));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_iter(20, 13) = %d\t", CNP_iter(20, 13));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_iter(27, 18) = %d\t", CNP_iter(27, 18));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNP_iter(35, 25) = %d\t", CNP_iter(35, 25));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	printf("\nAvec la fonction CNP de implémenté en programmation dynamique :\n");
	tps_init = clock();
	printf("CNPDynamique(5, 3) = %d\t\t\t", CNPDynamique(5, 3));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPDynamique(12, 9) = %d\t\t", CNPDynamique(12, 9));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPDynamique(15, 12) = %d\t\t", CNPDynamique(15, 12));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPDynamique(20, 13) = %d\t\t", CNPDynamique(20, 13));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPDynamique(27, 18) = %d\t\t", CNPDynamique(27, 18));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	tps_init = clock();
	printf("CNPDynamique(35, 25) = %d\t", CNPDynamique(35, 25));
	tps_fin = clock();
	tps_CPU = (tps_fin - tps_init);
	printf("Temps = %ld microsecondes\n", tps_CPU);

	printf("\nVotre clavier ne répond plus...\n");
	printf("Appuyez sur une touche pour continuer.\n");
	getchar();
}