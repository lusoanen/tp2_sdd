#include "../inc/file.h"

/*****************************************************/
/* creerFile()                                       */
/*                                                   */
/* Allocation mémoire d'une file de type             */
/* file_t et initialisation des valeurs si réussite. */
/*                                                   */
/* Input :                                           */
/* Output : pointeur sur l'élément de type file_t    */
/*****************************************************/

file_t *creerFile()
{
    file_t *file = (file_t *)malloc(sizeof(file_t));
    if (file == NULL)
    {
        fprintf(stderr, "Allocation mémoire échouée au niveau de la file\n");
        exit(1);
    }
    file->tab = (elem_t *)malloc(2 * sizeof(elem_t));
    if (file->tab == NULL)
    {
        fprintf(stderr, "Allocation mémoire échouée au niveau du tableau\n");
        exit(1);
    }
    file->deb = 0;
    file->fin = 0;
    file->tailleTab = 2;
    return file;
}

/*****************************************************/
/* enfiler()                                         */
/*                                                   */
/* Insert une nouvelle valeur, passée en paramètre   */
/* dans la file pointée, réalocation de la mémoire si*/
/* le tableau stockant la file est trop petit.       */
/*                                                   */
/* Input :  file (pointeur sur la file)              */
/*          valeur (valeur à inserer)                */
/* Output :                                          */
/*****************************************************/

void enfiler(file_t *file, elem_t valeur)
{
    int reallocAddNumber = 1;

    file->tab[file->fin] = valeur;

    if (file->fin == (file->tailleTab) - 1) //si l'indice de fin de file est arrivé à la fin du tableau
        file->fin = 0;
    else
        file->fin = (file->fin) + 1;

    if (file->fin == file->deb)
    { //si le tableau est trop petit (l'indice de fin de file à rejoind celui de début de file)
        file->tab = (elem_t *)realloc(file->tab, ((file->tailleTab) + reallocAddNumber) * sizeof(elem_t));
        if (file->tab == NULL)
        {
            fprintf(stderr, "Réallocation mémoire échouée au niveau de la file");
            exit(1);
        }

        file->tailleTab = (file->tailleTab) + reallocAddNumber;

        int i = (file->tailleTab) - 1;
        while (i != file->deb)
        { //Replacement des valeurs dans le tableau pour garder la cohérence de la liste
            file->tab[i] = file->tab[i - reallocAddNumber];
            i--;
        }
        file->deb = (file->deb) + reallocAddNumber;
    }
}

/*****************************************************/
/* défiler()                                         */
/*                                                   */
/* Avance l'indice de début de file dans le tableau, */
/* réalisant donc le défilement de la première valeur*/
/* insere dans la liste.                             */
/*                                                   */
/* Input :  file (pointeur sur la file)              */
/* Output :                                          */
/*****************************************************/

void defiler(file_t *file)
{
    if (file->deb == file->tailleTab - 1)
        file->deb = 0;
    else
        file->deb++;
}

/*****************************************************/
/* estVideFile()                                     */
/*                                                   */
/* Retourne si la file est vide (1) ou non (0) en    */
/* vérifiant si l'indice de début et de fin sont     */
/* égaux.                                            */
/*                                                   */
/* Input :  file (file à évaluer)                    */
/* Output :  entier de type bool_t                   */
/*****************************************************/

bool_t estVideFile(file_t file)
{
    return file.deb == file.fin;
}

/*****************************************************/
/* queueFile()                                       */
/*                                                   */
/* Renvoie la valeur de la queue de file c'est à dire*/
/* le prochain élement qui sera défilé.              */
/*                                                   */
/* Input :  file (file à évaluer)                    */
/* Output : valeur de l'élément en queue de file     */
/*****************************************************/

elem_t queueFile(file_t file)
{
    return file.tab[file.deb];
}

/*****************************************************/
/* libererFile()                                     */
/*                                                   */
/* libération de l'espace mémoire alloué au tableau  */
/* et à la file.                                     */
/*                                                   */
/* Input :  file (pointeur sur file)                 */
/* Output :                                          */
/*****************************************************/

void libererFile(file_t *file)
{
    free(file->tab);
    free(file);
}

/*****************************************************/
/* afficherFile()                                    */
/*                                                   */
/* Affiche les élements de la file du premier arrivé */
/* au dernier (de l'indice debut à l'indice fin).    */
/*                                                   */
/* Input :  file (file à évaluer)                    */
/* Output :                                          */
/*****************************************************/

void afficherFile(file_t file)
{
    int i = file.deb;
    while (i != file.fin)
    {
        printf("valeur : %d\n", file.tab[i]);
        if (i == file.tailleTab - 1) //si on est en fin de tableau on retourne au début
            i = 0;
        else
            i++;
    }
}
