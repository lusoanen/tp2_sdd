#include "../inc/liste.h"

/*****************************************************/
/* creerListe()                                      */
/*                                                   */
/* Allocation mémoire d'une liste de type            */
/* liste_t et initialisation du pointeur si réussite.*/
/*                                                   */
/* Input :                                           */
/* Output : pointeur sur l'élément de type liste_t   */
/*****************************************************/

liste_t *creerListe()
{
    liste_t *nouvelle = (liste_t *)malloc(sizeof(liste_t));
    if (nouvelle == NULL)
    {
        fprintf(stderr, "Allocation mémoire imposible de la liste.\n");
        exit(1);
    }

    nouvelle->tete = NULL;
    return nouvelle;
}

/*****************************************************/
/* viderListe()                                      */
/*                                                   */
/* suppression des éléments de la liste pointée un à */
/* un.                                               */
/*                                                   */
/* Input :  liste (pointeur sur liste)               */
/* Output :                                          */
/*****************************************************/

void viderListe(liste_t *liste)
{

    while (liste->tete != NULL) //tant que le pointeur sur la tête de liste ne pointe pas sur NULL
    {
        supprimerElement(&(liste->tete));
    }
    liste->tete = NULL;
}

/*****************************************************/
/* supprimerElement()                                 */
/*                                                   */
/* Libération de l'espace mémoire allouée à une      */
/* cellule. L'adresse du précédent est passée en     */
/* paramètre, ce qui permet le resspect du chainage  */
/* après suppression de l'élément                    */
/*                                                   */
/* Input :  prec (pointeur sur le pointeur de        */
/*          l'élément suivant du précédent)          */
/* Output :                                          */
/*****************************************************/

void supprimerElement(maillon_t **prec)
{
    if (*prec != NULL)
    {
        maillon_t *aSuppr = *prec;
        *prec = aSuppr->suiv;
        free(aSuppr);
    }
    else
    {
        fprintf(stderr, "Suppression de cellule impossible.\n");
        exit(1);
    }
}

/*****************************************************/
/* insererListe()                                    */
/*                                                   */
/* Insert une nouvelle valeur, passée en paramètre,  */
/* à la suite de l'élément donnée.                   */
/*                                                   */
/* Input :  prec (pointeur sur le pointeur de        */
/*          l'élément suivant du précédent)          */
/*          valeur (valeur à inserer)                */
/* Output :                                          */
/*****************************************************/

void insererListe(maillon_t **prec, elem_t valeur)
{
    maillon_t *cour = (maillon_t *)malloc(sizeof(maillon_t));
    if (cour == NULL)
    {
        fprintf(stderr, "Allocation mémoire de la cellule imposible.\n");
        exit(1);
    }
    cour->elem = valeur;
    cour->suiv = *prec;
    *prec = cour;
}

/*****************************************************/
/* afficherListe()                                   */
/*                                                   */
/* Affiche les élements de la liste                  */
/*                                                   */
/* Input :  file (file à évaluer)                    */
/* Output :                                          */
/*****************************************************/

void afficherListeInt(maillon_t *cour)
{
    while (cour != NULL) //tant qu'on a pas parcouru toute la liste
    {
        printf("Valeur : %d \n", cour->elem);
        cour = cour->suiv;
    }
}